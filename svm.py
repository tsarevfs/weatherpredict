import scipy
from scipy.optimize import fmin_bfgs

import numpy as np
from collections import Counter, namedtuple

accurancy = lambda res : (res.tp + res.tn) / (res.tp + res.tn + res.fp + res.fn)
precision = lambda res : float(res.tp) / (res.tp + res.fp)
recall = lambda res : float(res.tp) / (res.tp + res.fn)
f1 = lambda res : 2 * precision(res) * recall(res) / (precision(res) + recall(res))

def checkClassifier(classifier, X, y):
	t = {(1, 1) : 'tp', (-1, -1) : 'tn', (1, -1) : 'fn', (-1, 1) : 'fp'}
	c = Counter()
	for (xi, yi) in zip(X, y):
		yip = classifier(xi)
		c[t[(yip, yi)]] += 1
	return namedtuple('Result', ['tp', 'tn', 'fp', 'fn'])(c['tp'], c['tn'], c['fp'], c['fn'])

def split(X, y, n):
	perm = np.random.permutation(y.size)
	s, t = perm[:n], perm[n:]
	return X[s], y[s], X[t], y[t]

def loadData():
	fin = open('dataset.txt', 'r')
	lines = fin.readlines()

	n = int(lines[0])
	X = []
	y = []

	for i in range(n):
		inp = list([float(a) for a in lines[2 * i + 1].split(' ')])
		targ = float(lines[2 * i + 2])
		X.append(inp)
		y.append(targ)
	return (np.array(X), np.array(y))

def mkSVMClassifier(X, y, C = 1):
	n, m  = X.shape
	def f(theta_):
		theta, theta0 = theta_[:m], theta_[m]
		res = y * (np.dot(X, theta) + theta0)
		return .5 * sum(theta * theta) + C * sum(1 - res[res < 1])
	theta = fmin_bfgs(f, np.zeros(m + 1))
	return lambda v : np.sign(np.dot(np.hstack((v, 1)).T, theta)) or 1

def main():
	X, y = loadData()

	bestC, bestCF1, bestRes = None, None, None

	for C in 2. ** np.arange(-5, 20):
		XStudy, yStudy, XTest, yTest = split(X, y, int(0.5 * y.size))
		classifier = mkSVMClassifier(XStudy, yStudy, C)
		res = checkClassifier(classifier, XTest, yTest)
		print(res)
		f1Here = f1(res)
		print ("SMV with C=%f, F1=%f"%(C, f1Here))
		if not bestCF1 or f1Here > bestCF1:
			bestCF1, bestC, bestRes = f1Here, C, res

	for (w, f) in [('accurancy', accurancy), ('precision', precision), ('recall', recall), ('F1 measure', f1)]:
		print ("SVM with C=%f %s: %f"%(bestC, w, f(bestRes)))

if __name__ == '__main__':
	main()