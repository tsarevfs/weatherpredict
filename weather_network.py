from pybrain.tools.shortcuts import buildNetwork
from pybrain.structure import SigmoidLayer
from pybrain.datasets import SupervisedDataSet
from pybrain.datasets import ClassificationDataSet
from pybrain.supervised.trainers import BackpropTrainer	

from pybrain.utilities           import percentError
from math import *
import random

#alldata = SupervisedDataSet(10, 1)
alldata = ClassificationDataSet(9, class_labels=['not_warmer', 'warmer'])

fin = open('dataset.txt', 'r')
lines = fin.readlines()

n = int(lines[0])

for i in range(n):
	inp = [float(a) for a in lines[2 * i + 1].split(' ')]
	targ = (float(lines[2 * i + 2]) + 1) /2
	alldata.addSample(inp, (targ,))

tstdata, trndata = alldata.splitWithProportion( 0.25 )
trndata._convertToOneOfMany( )
tstdata._convertToOneOfMany( )

print "Number of training patterns: ", len(trndata)
print "Input and output dimensions: ", trndata.indim, trndata.outdim
print "First sample (input, target, class):"
print trndata['input'][0], trndata['target'][0], trndata['class'][0]

net = buildNetwork( trndata.indim, 8, trndata.outdim, hiddenclass=SigmoidLayer)
trainer = BackpropTrainer(net, trndata, learningrate=0.003)

for i in range(50):
	trnresult = percentError( trainer.testOnClassData(),
	                          trndata['class'] )
	tstresult = percentError( trainer.testOnClassData(
	       dataset=tstdata ), tstdata['class'] )

	print "epoch: %4d" % trainer.totalepochs, \
	      "  train error: %5.2f%%" % trnresult, \
	      "  test error: %5.2f%%" % tstresult

	trainer.trainEpochs(1)
