#!/usr/bin/python
#-*- coding: utf-8 -*-

import time
import sys
from math import *

def get_direction(s):
	if len(s) == 0:
		return 0
	wd = s.decode('utf-8')

	if wd.startswith(u'Штиль'):
		return 0

	if wd.startswith(u'Ветер, дующий с '):
		dir_srt = wd[16:]
		components = [c[0] for c in dir_srt.split(u'-')]

		if len(components) > 1:
			components[-2] += components[-1]
			components.pop()

		dir2angle = {u'с' : 0, u'св' : 45, u'в' : 90, u'юв' : 135, u'ю' : 180, u'юз' : 225, u'з' : 270, u'сз' : 315}
		

		comp_angles = [dir2angle[comp] for comp in components]
		if len(comp_angles) == 2 and comp_angles[0] == 0 and comp_angles[1] == 315:
			comp_angles[0] = 360

		angle = float(sum(comp_angles)) / len(comp_angles)
		return angle


def normalized(value, average, max_val):
	return (value - average) / max_val

def cycled_data(value, period):
	nvalue = value / period * 2 * pi
	return (sin(nvalue), cos(nvalue))

def main():
	inp = open('weather.txt')
	records = inp.readlines()

	test_limit = 0;

	last_T = None
	last_P = None
	last_H = None

	summ_T = 0
	summ_P = 0
	summ_H = 0

	max_T = 0
	max_P = 0
	max_H = 0

	converted_items = []

	for record in records:
		test_limit += 1
		if test_limit > 300:
			pass
			#break

		items = [field[1:-1] for field in record.split(';')]

		#try:
		date =  time.strptime(items[0], '%d.%m.%Y %H:%M')

		if len(items[1]) > 0:
			T = float(items[1])
			last_T = T
		else:
			T = last_T
		summ_T += T
		max_T = max(max_T, abs(T))

		if len(items[3]) > 0:
			P = float(items[3])
			last_P = P
		else:
			P = last_P
		summ_P += P
		max_P = max(max_P, abs(P))

		if len(items[5]) > 0:
			H = float(items[5])
			last_H = H
		else:
			H = last_H
		summ_H += H
		max_H = max(max_H, abs(H))
		
		wind_direction = get_direction(items[6])

		converted_items.append((date.tm_yday, date.tm_hour, T, P, H, wind_direction))

		# except Exception, e:
		# 	print e.message
		# 	print items

	#print converted_items
	print max_T
	print summ_T / len(converted_items)

	fout = open('dataset.txt', 'w')

	dataset = []

	for i, (yd, day_time, T, P, H, wd) in enumerate(converted_items):
		day_ago = None
		day_forward = None

		if not 20 < i < len(converted_items) - 20:
			continue

		if converted_items[i - 8][1] == day_time and yd + 1 == converted_items[i - 8][0] % 365:
			day_forward = 8
		if converted_items[i + 8][1] == day_time and yd % 365 == converted_items[i + 8][0] + 1:
			day_back = 8

		if day_forward == None or day_back == None:
			continue

		t_next_day = converted_items[i - day_forward][2]
		t_prev_day = converted_items[i + day_back][2]
		day_data = cycled_data(float(yd), 365.)
		time_data = cycled_data(float(day_time), 24.)

		nT = normalized(T, summ_T / len(converted_items), max_T)
		npT = normalized(t_prev_day, summ_T / len(converted_items), max_T)
		nP = normalized(P, summ_P / len(converted_items), max_P)
		nH = normalized(H, summ_H / len(converted_items), max_H)

		wd_data = cycled_data(wd, 360.)

		inp = (day_data[0], day_data[1], time_data[0], time_data[1], wd_data[0], wd_data[1], nT, nP, npT)
		#inp = (day_data[0], day_data[1], wd_data[0], wd_data[1], nT, nP, nH, npT)
		targ = None
		if t_next_day - T > 0:
			targ = 1
		else:
			targ = -1

		dataset.append((inp, targ))

	fout.write(str(len(dataset)) + '\n')

	for inp, targ in dataset:
		fout.write(' '.join([str(a) for a in inp]) + '\n')
		fout.write(str(targ) + '\n')

	fout.close()


if __name__ == '__main__':
	main()